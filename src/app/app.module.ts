// General Modules
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';

// Components
import { AppComponent } from './app.component';
import { HomeComponent } from './pages/home/home.component';
import { NotificationsComponent } from './pages/notifications/notifications.component';
import { SearchComponent } from './pages/search/search.component';
import { GroupsComponent } from './pages/groups/groups.component';
import { ProfileComponent } from './pages/profile/profile.component';
import { LoginComponent } from './pages/login/login.component';
import { RegisterComponent } from './pages/register/register.component';
import { SocialLoginComponent } from './shared-components/social-login/social-login.component';
import { CardComponent } from './shared-components/card/card.component';
import { PostComponent } from './shared-components/post/post.component';
import { HeaderComponent } from './shared-components/header/header.component';
import { FooterComponent } from './shared-components/footer/footer.component';
import { SearchFormComponent } from './shared-components/search-form/search-form.component';
import { HeaderSecundaryComponent } from './shared-components/header-secundary/header-secundary.component';
import { ProductsComponent } from './pages/products/products.component';
import { ProductDetailComponent } from './pages/product-detail/product-detail.component';
import { PostsComponent } from './pages/posts/posts.component';
import { PostDetailComponent } from './pages/post-detail/post-detail.component';
import { FavoritesComponent } from './pages/favorites/favorites.component';
import { ProfileEditComponent } from './pages/profile-edit/profile-edit.component';
import { ResetPasswordComponent } from './pages/reset-password/reset-password.component';
import { ChangePasswordComponent } from './pages/change-password/change-password.component';
import { NewPostComponent } from './pages/new-post/new-post.component';
import { NewAccountComponent } from './pages/new-account/new-account.component';
import { LocationComponent } from './shared-components/location/location.component';
import { NewProductComponent } from './pages/new-product/new-product.component';
import { NewGroupComponent } from './pages/new-group/new-group.component';
import { OpinionsProductComponent } from './pages/opinions-product/opinions-product.component';
import { ChatComponent } from './pages/chat/chat.component';
import { GroupDetailComponent } from './pages/group-detail/group-detail.component';

// External Services
import { CarouselModule } from 'ngx-owl-carousel-o';
import { ToastrModule } from 'ngx-toastr';

// Interceptors
import { TokenInterceptorProvider } from './interceptors/token.interceptor';
import { CacheInterceptorProvider } from './interceptors/cache.interceptor';

// Services
import { RequestCacheService } from './services/request-cache.service';

// Material
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';

const components = [
  AppComponent,
  HomeComponent,
  NotificationsComponent,
  SearchComponent,
  GroupsComponent,
  ProfileComponent,
  LoginComponent,
  RegisterComponent,
  SocialLoginComponent,
  CardComponent,
  PostComponent,
  HeaderComponent,
  FooterComponent,
  SearchFormComponent,
  HeaderSecundaryComponent,
  ProductsComponent,
  ProductDetailComponent,
  PostsComponent,
  PostDetailComponent,
  FavoritesComponent,
  ProfileEditComponent,
  ResetPasswordComponent,
  ChangePasswordComponent,
  NewPostComponent,
  NewAccountComponent,
  LocationComponent,
  NewProductComponent,
  NewGroupComponent,
  OpinionsProductComponent,
  ChatComponent,
  GroupDetailComponent,
];

const interceptors = [
  TokenInterceptorProvider,
  CacheInterceptorProvider
];

const services = [
  RequestCacheService,
];

const material = [
  MatProgressSpinnerModule,
];

const toastr = ToastrModule.forRoot(
  {
    timeOut: 3000,
    positionClass: 'toast-top-center',
    preventDuplicates: true,
  }
);

@NgModule({
  declarations: [
    components,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    CarouselModule,
    ReactiveFormsModule,
    HttpClientModule,
    material,
    toastr,
  ],
  providers: [interceptors, services],
  bootstrap: [AppComponent]
})
export class AppModule { }
