import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './pages/home/home.component';
import { LoginComponent } from './pages/login/login.component';
import { RegisterComponent } from './pages/register/register.component';
import { NotificationsComponent } from './pages/notifications/notifications.component';
import { SearchComponent } from './pages/search/search.component';
import { GroupsComponent } from './pages/groups/groups.component';
import { ProfileComponent } from './pages/profile/profile.component';
import { ProductsComponent } from './pages/products/products.component';
import { ProductDetailComponent } from './pages/product-detail/product-detail.component';
import { PostsComponent } from './pages/posts/posts.component';
import { PostDetailComponent } from './pages/post-detail/post-detail.component';
import { FavoritesComponent } from './pages/favorites/favorites.component';
import { ProfileEditComponent } from './pages/profile-edit/profile-edit.component';
import { ResetPasswordComponent } from './pages/reset-password/reset-password.component';
import { ChangePasswordComponent } from './pages/change-password/change-password.component';
import { NewPostComponent } from './pages/new-post/new-post.component';
import { NewAccountComponent } from './pages/new-account/new-account.component';
import { NewProductComponent } from './pages/new-product/new-product.component';
import { NewGroupComponent } from './pages/new-group/new-group.component';
import { OpinionsProductComponent } from './pages/opinions-product/opinions-product.component';
import { ChatComponent } from './pages/chat/chat.component';
import { GroupDetailComponent } from "./pages/group-detail/group-detail.component";
import { AuthGuard } from './guards/auth.guard';
import { LoggedGuard } from './guards/logged.guard';

const routes: Routes = [
  { path: 'home', component: HomeComponent, canActivate: [AuthGuard] },
  { path: 'search', component: SearchComponent, canActivate: [AuthGuard]  },
  { path: 'groups', component: GroupsComponent, canActivate: [AuthGuard]  },
  { path: 'posts', component: PostsComponent, canActivate: [AuthGuard]  },
  { path: 'post-detail/:id', component: PostDetailComponent, canActivate: [AuthGuard]  },
  { path: 'detail-group/:id', component: GroupDetailComponent, canActivate: [AuthGuard]  },
  { path: 'favorites', component: FavoritesComponent, canActivate: [AuthGuard]  },
  { path: 'profile', component: ProfileComponent, canActivate: [AuthGuard]  },
  { path: 'profile-edit', component: ProfileEditComponent, canActivate: [AuthGuard]  },
  { path: 'products', component: ProductsComponent, canActivate: [AuthGuard]  },
  { path: 'product-detail', component: ProductDetailComponent, canActivate: [AuthGuard]  },
  { path: 'notifications', component: NotificationsComponent, canActivate: [AuthGuard]  },
  { path: 'new-post', component: NewPostComponent, canActivate: [AuthGuard]  },
  { path: 'new-account', component: NewAccountComponent, canActivate: [AuthGuard]  },
  { path: 'new-product', component: NewProductComponent, canActivate: [AuthGuard]  },
  { path: 'new-group', component: NewGroupComponent, canActivate: [AuthGuard]  },
  { path: 'opinions-product', component: OpinionsProductComponent, canActivate: [AuthGuard]  },
  { path: 'chat/:id_user', component: ChatComponent, canActivate: [AuthGuard]  },

  { path: 'login', component: LoginComponent, canActivate: [LoggedGuard]   },
  { path: 'register', component: RegisterComponent, canActivate: [LoggedGuard]   },
  { path: 'reset-password', component: ResetPasswordComponent },
  { path: 'change-password', component: ChangePasswordComponent },
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: '**', component: HomeComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
