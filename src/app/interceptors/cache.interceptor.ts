import { Injectable } from '@angular/core';
import {
  HTTP_INTERCEPTORS,
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpHeaders,
  HttpResponse,
} from '@angular/common/http';
import { Subscriber, Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import {RequestCacheService} from '../services/request-cache.service';

@Injectable()
export class CacheInterceptor implements HttpInterceptor {

  constructor(private _cache: RequestCacheService) {}

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    // return next.handle(request);
    if (req.method !== 'GET' || this._isDecache(req)) {
      this._cache.deCache(req);
    }

    return new Observable((observer: Subscriber<any>) => {
      const onSuccess = (response: HttpEvent<any>) => observer.next(response);
      const onError = (error: any) => observer.error(error);
      const onComplete = () => observer.complete();

      // continue if not cachable.
      if (!this._isCachable(req) && !this._isDoubleCache(req)) {
        next.handle(req).subscribe(onSuccess, onError, onComplete);
      } else {
        const cachedResponse = this._cache.get(req);

        if (this._isDoubleCache(req)) {
          // Double cache
          if (cachedResponse) {
            observer.next(cachedResponse);
          }
          this._sendRequest(req, next, this._cache).subscribe(onSuccess, onError, onComplete);
        } else if (this._isCachable(req)) {
          // Cache
          if (cachedResponse) {
            observer.next(cachedResponse);
            observer.complete();
          } else {
            this._sendRequest(req, next, this._cache).subscribe(onSuccess, onError, onComplete);
          }
        }
      }
    });

  }

  private _sendRequest(req: HttpRequest<any>, next: HttpHandler, cache: RequestCacheService): Observable<HttpEvent<any>> {
    const headerObject = { };

    // Remove cache, double cache or decache from http header
    req.headers.keys().forEach((key: string) => {
      if (key !== 'cache' && key !== 'doubleCache' && key !== 'decache') {
        (headerObject as any)[key] = req.headers.get(key);
      }
    });

    // create no cache header
    const noCacheHeaderReq: HttpRequest<any> = (req = req.clone({
      headers: new HttpHeaders(headerObject),
    }));

    // Return request
    if (noCacheHeaderReq) {
      return next.handle(noCacheHeaderReq).pipe(
        tap((event: HttpEvent<any>) => {
          if (event instanceof HttpResponse) {
            cache.set(req, event);
          }
        }),
      );
    }

    return next.handle(req);
  }

  private _isCachable(req: HttpRequest<any>): boolean {
    return req.headers.get('cache') === 'true';
  }

  private _isDoubleCache(req: HttpRequest<any>): boolean {
    return req.headers.get('doubleCache') === 'true';
  }

  private _isDecache(req: HttpRequest<any>): boolean {
    return req.headers.get('decache') === 'true';
  }

}

export const CacheInterceptorProvider = {
  provide: HTTP_INTERCEPTORS,
  useClass: CacheInterceptor,
  multi: true,
};
