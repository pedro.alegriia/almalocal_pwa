import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from '../services/auth.service';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  loading = false;
  constructor(private _router: Router, private _authService: AuthService) { }

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {

    this.loading = true;
    return new Promise(resolve => {
      this._authService.loadSession().then(success => {
        this.loading = false;
        resolve(true);
      }, failure => {
        this.loading = false;
        const tree: UrlTree = this._router.parseUrl('login');
        resolve(tree);
      });
    });
  }

}
