import { Injectable } from '@angular/core';
import { ApiService } from './api.service';

@Injectable({
  providedIn: 'root'
})
export class CategoryService{

  constructor(private _apiService: ApiService) { }

  getCategories(){
    return this._apiService.get('category');
  }

  getHomeBanner(){
    return this._apiService.get('home_banner', {cache: true});
  }

}
