import { Injectable } from '@angular/core';
import { HttpHeaders, HttpParams, HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  urlApi = environment.url_api;

  constructor(private _http: HttpClient) { }

 /**
   * GET request
   * @param url
   * @param options
   */
  get(url: string, options?: {
    headers?: HttpHeaders | {
      [header: string]: string | string[];
    };
    observe?: any;
    params?: HttpParams | {
      [param: string]: string | string[];
    };
    reportProgress?: boolean;
    responseType?: any;
    withCredentials?: boolean;
    cache?: boolean; // Caching http response for better performance
    doubleCache?: boolean; // return cache then return real response
    decache?: boolean // decache for the cache and doubleCache
  }): Observable<any> {
    return this._http.get(this._formatUrl(url), this._formatOption(options));
  }

  /**
   * POST request
   * @param url
   * @param body
   * @param options
   */
  post(url: string, body?: any | null, options?: {
    headers?: HttpHeaders | {
      [header: string]: string | string[];
    };
    observe?: any;
    params?: HttpParams | {
      [param: string]: string | string[];
    };
    reportProgress?: boolean;
    responseType?: any;
    withCredentials?: boolean;
  }): Observable<any> {
    return this._http.post(
      this._formatUrl(url),
      body,
      this._formatOption(options),
    );
  }

  /**
   * PATCH request
   * @param url
   * @param body
   * @param options
   */
  patch(url: string, body: any | null, options?: {
    headers?: HttpHeaders | {
      [header: string]: string | string[];
    };
    observe?: any;
    params?: HttpParams | {
      [param: string]: string | string[];
    };
    reportProgress?: boolean;
    responseType?: any;
    withCredentials?: boolean;
  }): Observable<any> {
    return this._http.patch(
      this._formatUrl(url),
      body,
      this._formatOption(options),
    );
  }

  /**
   * PUT request
   * @param url
   * @param body
   * @param options
   */
  put(url: string, body: any | null, options?: {
    headers?: HttpHeaders | {
      [header: string]: string | string[];
    };
    observe?: any;
    params?: HttpParams | {
      [param: string]: string | string[];
    };
    reportProgress?: boolean;
    responseType?: any;
    withCredentials?: boolean;
  }): Observable<any> {
    return this._http.put(
      this._formatUrl(url),
      body,
      this._formatOption(options),
    );
  }

  /**
   * DELETE request
   * @param url
   * @param options
   */
  delete(url: string, options?: {
    headers?: HttpHeaders | {
      [header: string]: string | string[];
    };
    observe?: any;
    params?: HttpParams | {
      [param: string]: string | string[];
    };
    reportProgress?: boolean;
    responseType?: any;
    withCredentials?: boolean;
  }): Observable<any> {
    return this._http.delete(this._formatUrl(url), this._formatOption(options));
  }


  private _formatUrl(url: string) {
    if (!url) {
      throw new Error('Invaid URL');
    }
    if (url.includes('http')) {
      return url;
    }

    if (url.length > 0) {
      if (url.charAt(0) === '/') {
        url = url.substring(1);
      }
    }

    return `${this.urlApi}${url}`;
  };

  private _formatOption(options?: any) {
    const formatted = { ...options };
    if (options) {

      // If options has cache = true, remove from option and put into headers
      if (options.cache) {
        delete formatted.cache;
        formatted.headers = { ...formatted.headers, cache: 'true' };
      }

      // If options has doubleCache = true, remove from option and put into headers
      if (options.doubleCache) {
        delete formatted.doubleCache;
        formatted.headers = { ...formatted.headers, doubleCache: 'true' };
      }

      // If options has decache = true, remove from option and put into headers
      if (options.decache) {
        delete formatted.decache;
        formatted.headers = { ...formatted.headers, decache: 'true' };
      }
    }
    return formatted;
  }

}
