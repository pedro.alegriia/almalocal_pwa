import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  token = null;

  constructor(
    public http: HttpClient,
    public apiService: ApiService,
  ) { }

  loadSession(): Promise<void> {
    return new Promise((resolve, reject) => {
      if (this.token){
        resolve();
      }
      else {
        this.apiService.get('auth/token/', { withCredentials: true }).subscribe(res => {
          this.token = res.token;
          if (this.token){
            resolve();
          } else {
            reject();
          }
        }, err => {
          console.log(err);
          reject();
        });
      }
    });
  }

  getHeaderAuthorization(): string {
    return this.token ? `Token ${this.token}` : '';
  }

  login(data: any) {
    return this.apiService.post('auth/login/', data, { withCredentials: true });
  }

  register(data: any) {
    return this.apiService.post('auth/register/', data, { withCredentials: true });
  }

  googleInit(register= false) {
    window.location.href = `${this.apiService.urlApi}google/auth/?register=${register}`;
  }

}
