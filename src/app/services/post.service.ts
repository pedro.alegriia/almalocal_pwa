import { Injectable } from '@angular/core';
import { ApiService } from './api.service';

@Injectable({
  providedIn: 'root'
})
export class PostService{
  private endpoint:string = 'post';

  constructor(private _apiService: ApiService) { }

  getAllPosts(){
    return this._apiService.get(this.endpoint);
  }

  getDetailPost(idProd: number){
    return this._apiService.get(this.endpoint +'/'+ idProd);
  }

  savePot(params: any){
    return this._apiService.post(this.endpoint, params);
  }
}
