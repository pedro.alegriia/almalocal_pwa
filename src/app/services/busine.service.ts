import { Injectable } from '@angular/core';
import { ApiService } from './api.service';

@Injectable({
  providedIn: 'root'
})
export class BusineService{
  private endpoint:string = 'business';

  constructor(private _apiService: ApiService) { }

  getAllBusines(){
    return this._apiService.get(this.endpoint);
  }

  getDetailBusines(idProd: number){
    return this._apiService.get(this.endpoint +'/'+ idProd);
  }

  saveBusine(params: any){
    return this._apiService.post(this.endpoint, params);
  }
}
