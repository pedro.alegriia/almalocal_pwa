import { Injectable } from '@angular/core';
import { ApiService } from './api.service';

@Injectable({
  providedIn: 'root'
})
export class CommunityService{
  private endpoint:string = 'community';

  constructor(private _apiService: ApiService) { }

  getAllCommunities(){
    return this._apiService.get(this.endpoint);
  }

  getDetailCommunity(idProd: number){
    return this._apiService.get(this.endpoint +'/'+ idProd);
  }

  saveCommunity(params: any){
    return this._apiService.post(this.endpoint, params);
  }
}
