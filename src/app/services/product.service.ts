import { Injectable } from '@angular/core';
import { ApiService } from './api.service';

@Injectable({
  providedIn: 'root'
})
export class ProductService{
  private endpoint:string = 'product';

  constructor(private _apiService: ApiService) { }

  getAllProducts(){
    return this._apiService.get(this.endpoint);
  }

  getDetailProduct(idProd: number){
    return this._apiService.get(this.endpoint +'/'+ idProd);
  }

  saveProduct(params: any){
    return this._apiService.post(this.endpoint, params);
  }
}
