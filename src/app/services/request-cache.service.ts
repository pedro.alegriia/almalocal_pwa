import { HttpRequest, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable()
export class RequestCacheService {
  private _map: Map<string, HttpResponse<any>>;
  private _urlSet: Set<string>;

  constructor() {
    this._map = new Map();
    this._urlSet = new Set();
  }

  deCache(req: HttpRequest<any>): void {
    if (this._urlSet.has(req.url)) {
      Array.from(this._map.keys()).forEach(key => {
        if (key.includes(req.url)) {
          this._map.delete(key);
        }
      });
    }
  }

  set(req: HttpRequest<any>, event: HttpResponse<any>): void {
    this._map.set(req.urlWithParams, event);
    this._urlSet.add(req.url);
  }

  get(req: HttpRequest<any>): HttpResponse<any> | undefined {
    return this._map.get(req.urlWithParams);
  }

}
