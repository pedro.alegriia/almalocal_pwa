import { Injectable } from '@angular/core';
import { ApiService } from './api.service';

@Injectable()
export class UserService {
  user = null;
  constructor(private _apiService: ApiService) {
    this.loadUserInfo();
  }



  public getCommunities(){
    return this._apiService.get('user/communities');
  }

  public getFavoritesProducts(){
    return this._apiService.get('user/favorite_products');
  }

  public loadUserInfo(){
    return new Promise(resolve => {
      if (this.user){
        resolve(this.user);
        return;
      }
      this._apiService.get('user/me').subscribe(res => {
        this.user = res;
        resolve(this.user);
      }, err => {
        console.log('Error loading user: ' + err);
        resolve(this.user);
      })
    });
  }

}
