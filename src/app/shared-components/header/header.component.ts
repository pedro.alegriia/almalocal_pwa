import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
declare var $: any;

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.sass']
})
export class HeaderComponent implements OnInit {
  menuActive: string;

  constructor(private router: Router) {
    this.menuActive = this.router.url.split('/')[1];
  }

  ngOnInit(): void {
  }

  gotTo(link: string){
    $('#modalopAdd').modal('hide');
    this.router.navigate([link]);
  }
}
