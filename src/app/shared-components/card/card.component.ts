import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.sass']
})
export class CardComponent implements OnInit {
  @Input() data: any;

  constructor() { }

  ngOnInit(): void {
    this.data.image = 'https://mdbootstrap.com/img/new/standard/city/041.jpg';
  }

}
