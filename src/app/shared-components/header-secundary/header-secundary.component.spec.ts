import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HeaderSecundaryComponent } from './header-secundary.component';

describe('HeaderSecundaryComponent', () => {
  let component: HeaderSecundaryComponent;
  let fixture: ComponentFixture<HeaderSecundaryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HeaderSecundaryComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderSecundaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
