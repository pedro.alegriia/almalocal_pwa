import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-header-secundary',
  templateUrl: './header-secundary.component.html',
  styleUrls: ['./header-secundary.component.sass']
})
export class HeaderSecundaryComponent implements OnInit {
  @Input() headerConf: any;
  constructor() { }

  ngOnInit(): void {
  }

}
