import { Component, OnInit, Input } from '@angular/core';
import { AuthService } from "../../services/auth.service";

@Component({
  selector: 'app-social-login',
  templateUrl: './social-login.component.html',
  styleUrls: ['./social-login.component.sass']
})
export class SocialLoginComponent implements OnInit {
  @Input() register = false;

  constructor(
    public authService: AuthService,
  ) { }

  ngOnInit(): void {
  }

}
