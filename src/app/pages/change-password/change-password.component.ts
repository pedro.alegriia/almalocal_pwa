import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.sass']
})
export class ChangePasswordComponent implements OnInit {
  headerConf = {
    route: 'profile',
    title: 'Contraseña'
  };

  constructor() { }

  ngOnInit(): void {
  }

}
