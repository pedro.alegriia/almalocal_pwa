import { Component, OnInit, AfterContentChecked } from '@angular/core';
import { OwlOptions } from 'ngx-owl-carousel-o';

@Component({
  selector: 'app-group-detail',
  templateUrl: './group-detail.component.html',
  styleUrls: ['./group-detail.component.sass']
})
export class GroupDetailComponent implements OnInit, AfterContentChecked {
  productsList: any;

  public customOptions: OwlOptions = {
    loop: false,
    mouseDrag: true,
    touchDrag: false,
    pullDrag: false,
    dots: false,
    navSpeed: 700,
    navText: ['', ''],
    items: 1,
    responsive: {
      0: {
        items: 1
      },
      400: {
        items: 2
      },
      740: {
        items: 2
      },
      940: {
        items: 3
      }
    },
    nav: true
  };

  gallery: any = [
    'https://cnet2.cbsistatic.com/img/iTWLlWeHdckQIX0vZdVOUfoKQvs=/940x0/2019/06/19/29e63aac-dc98-47fe-b82a-08b34ae15d35/smart-home-timelapse-1-3-497077.png',
    'https://cnet2.cbsistatic.com/img/iTWLlWeHdckQIX0vZdVOUfoKQvs=/940x0/2019/06/19/29e63aac-dc98-47fe-b82a-08b34ae15d35/smart-home-timelapse-1-3-497077.png'
  ];


  constructor() {
    this.productsList = {
      title: 'Zapatos Biodegradables',
      desc: '$525 MXN',
      sharelike: true,
      image: 'https://bootstrap-ecommerce.com/bootstrap-ecommerce-html/images/items/12.jpg'
    };
  }

  ngOnInit(): void {
  }

  ngAfterContentChecked() {
    const divsToHide: any = document.getElementsByClassName('owl-nav') as HTMLCollectionOf<HTMLElement>;
    for(let i = 0; i < divsToHide.length; i++){
      let node = divsToHide[i];
      node.style.visibility = 'hidden'; // or
      node.style.display = 'none';
    }
  }

}
