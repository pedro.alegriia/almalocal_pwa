import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-profile-edit',
  templateUrl: './profile-edit.component.html',
  styleUrls: ['./profile-edit.component.sass']
})
export class ProfileEditComponent implements OnInit {
  headerConf = {
    route: 'profile',
    title: 'Editar información'
  };

  constructor() { }

  ngOnInit(): void {
  }

}
