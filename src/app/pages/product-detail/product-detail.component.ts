import { Component, OnInit } from '@angular/core';
import { OwlOptions } from 'ngx-owl-carousel-o';
import {Router} from '@angular/router';

@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.sass']
})
export class ProductDetailComponent implements OnInit {
  headerConf = {
    route: 'products',
    title: 'Producto'
  };

  public customOptions: OwlOptions = {
    loop: false,
    mouseDrag: true,
    touchDrag: false,
    pullDrag: false,
    dots: false,
    navSpeed: 700,
    navText: ['', ''],
    items: 1,
    responsive: {
      0: {
        items: 1
      },
      400: {
        items: 1
      },
      740: {
        items: 1
      },
      940: {
        items: 1
      }
    },
    nav: true
  };

  gallery: any = [
      'https://cnet2.cbsistatic.com/img/iTWLlWeHdckQIX0vZdVOUfoKQvs=/940x0/2019/06/19/29e63aac-dc98-47fe-b82a-08b34ae15d35/smart-home-timelapse-1-3-497077.png',
      'https://cnet2.cbsistatic.com/img/iTWLlWeHdckQIX0vZdVOUfoKQvs=/940x0/2019/06/19/29e63aac-dc98-47fe-b82a-08b34ae15d35/smart-home-timelapse-1-3-497077.png'
      ];

  constructor(private router: Router) { }

  ngOnInit(): void {
  }

  goBack() {
    this.router.navigate(['products']);
  }

  showOpinions(){
    this.router.navigate(['opinions-product']);
  }

}
