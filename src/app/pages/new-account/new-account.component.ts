import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-new-post',
  templateUrl: './new-account.component.html',
  styleUrls: ['./new-account.component.sass']
})
export class NewAccountComponent implements OnInit {
  headerConf = {
    route: 'home',
    title: 'Configurar'
  };

  accountType: string;

  categories: any = [
    'Bienestar y Ssalud',
    'Restaurantes',
    'Centros de Reciclaje o Acopio',
    'Gratis',
    'Ropa y Accesorios', 'Hogar',
    'Alimentos y Bebida',
    'Bebé y Niños',
    'Cuidado Personalñ y Belleza'
  ];
  categoriesChecked: any = [];

  lifeStyles: any = [
    'Artesanal',
    'Veganos',
    'Organicos',
    'Reciduos Cero',
    'Sin Gluten', 'Keto',
    'Hecho a mano',
    'Hecho en casa',
    'Biodegradable',
    'Vegetariano'
  ];
  lifeStylesChecked: any = [];

  progrressSteps: number = 1;

  constructor(private router: Router) {
    this.accountType = '';
  }

  ngOnInit(): void {
  }

  nextSave() {
    if (this.progrressSteps < 4) {
      this.progrressSteps += 1;
    } else {
      this.router.navigate(['home']);
    }
  }

  selectAccountType(type: string) {
    this.accountType = type;
  }

  checkCategory(category: any, type: string) {
    if (type == 'styles') {
      if (this.lifeStylesChecked.includes(category)) {
        this.lifeStylesChecked.splice(category, 1);
      } else {
        this.lifeStylesChecked.push(category)
      }
    } else {
      if (this.categoriesChecked.includes(category)) {
        this.categoriesChecked.splice(category, 1);
      } else {
        this.categoriesChecked.push(category)
      }
    }

  }

  activeInactive(index: any, type: string) {
    let status = false;
    if (type == 'styles') {
      if (this.lifeStylesChecked.includes(index)) {
        status = true
      }
    } else {
      if (this.categoriesChecked.includes(index)) {
        status = true
      }
    }
    return status
  }
}
