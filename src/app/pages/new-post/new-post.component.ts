import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-new-post',
  templateUrl: './new-post.component.html',
  styleUrls: ['./new-post.component.sass']
})
export class NewPostComponent implements OnInit {
  headerConf = {
    route: 'home',
    title: 'Nuevo Post'
  };



  constructor(private router: Router) { }

  ngOnInit(): void {
  }

  publish(){
    this.router.navigate(['home']);
  }

}
