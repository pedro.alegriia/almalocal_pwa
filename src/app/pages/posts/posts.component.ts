import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {PostService} from "../../services/post.service";

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.sass']
})
export class PostsComponent implements OnInit {
  postsList: any;

  headerConf = {
    route: 'profile',
    title: 'Publicaciones'
  };

  constructor(
      private router: Router,
      private postService: PostService,
  ) { }

  ngOnInit(): void {
    this.loadpots();
  }

  showDetail(idPost: number): void {
    this.router.navigate(['/post-detail/'+ idPost]);
  }

  loadpots(){
    this.postService.getAllPosts().subscribe((res: any) => {
          if (res && res.hasOwnProperty('count') && res['count'] >0 ){
            this.postsList = res['results'];
          }
        },
        error => {
          console.log(error);
        })
  }
}
