import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from "../../services/auth.service";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.sass']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;

  constructor(public authService: AuthService, public formBuilder: FormBuilder,
              private router: Router
  ) {
    this.loginForm = this.formBuilder.group(
      {
        username: ["", [Validators.required]],
        password: ["", [Validators.required, Validators.minLength(6)]],
      }
    );
  }

  ngOnInit(): void {
  }

  login() {
    if (this.loginForm.invalid) {
      return;
    }

    const data = this.loginForm.value;
    this.authService.login(data).subscribe(res => {
        console.log(res);
        location.reload();
      },
      error => {
        console.log(error)
      })

  }

}
