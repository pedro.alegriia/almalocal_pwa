import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";

@Component({
  selector: 'app-favorites',
  templateUrl: './favorites.component.html',
  styleUrls: ['./favorites.component.sass']
})
export class FavoritesComponent implements OnInit {
  headerConf = {
    route: 'profile',
    title: 'Favoritos'
  };

  products = [
    {
      title: 'Fresas Frescas',
      desc: '4.6 km',
      sharelike: true,
      favorite: true,
      image: 'https://image.shutterstock.com/image-photo/strawberry-shooting-isolated-on-white-600w-1627743610.jpg'
    },
    {
      title: 'Verduras frescas',
      desc: '7.6 km',
      sharelike: true,
      favorite: true,
      image: 'https://image.shutterstock.com/image-photo/portion-various-eggplants-two-same-600w-1811286649.jpg'
    }];

  constructor(private router: Router) { }

  ngOnInit(): void {
  }

  showDetail(): void {
    this.router.navigate(['/product-detail']);
  }
}
