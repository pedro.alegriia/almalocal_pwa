import { AfterContentChecked, Component, OnInit } from '@angular/core';
import { OwlOptions } from 'ngx-owl-carousel-o';
import { CategoryService } from '../../services/category.service';
import { BusineService } from '../../services/busine.service';
import { ProductService } from '../../services/product.service';
import { CommunityService } from '../../services/community.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.sass']
})
export class HomeComponent implements OnInit, AfterContentChecked {
  categoriesList: any;
  businessList: any;
  productsList: any;
  communitiesList: any;
  banners: any;

  public customOptionsCat: OwlOptions = {
    loop: false,
    mouseDrag: true,
    touchDrag: true,
    pullDrag: true,
    dots: false,
    navSpeed: 700,
    navText: ['', ''],
    responsive: {
      0: {
        items: 3
      },
      400: {
        items: 5
      },
      740: {
        items: 7
      },
      940: {
        items: 9
      }
    },
    nav: true
  };
  public customOptionsB: OwlOptions = {
    loop: false,
    mouseDrag: true,
    touchDrag: true,
    pullDrag: true,
    dots: false,
    navSpeed: 700,
    navText: ['', ''],
    responsive: {
      0: {
        items: 1
      },
      400: {
        items: 2
      },
      740: {
        items: 3
      },
      940: {
        items: 4
      }
    },
    nav: true
  };
  public customOptions: OwlOptions = {
    loop: false,
    mouseDrag: true,
    touchDrag: false,
    pullDrag: false,
    dots: false,
    navSpeed: 700,
    navText: ['', ''],
    responsive: {
      0: {
        items: 1
      },
      400: {
        items: 2
      },
      740: {
        items: 3
      },
      940: {
        items: 4
      }
    },
    nav: true
  };

  data: any;
  data2: any;
  constructor(
      private categorySvc: CategoryService,
      private businessSvc: BusineService,
      private productSvc: ProductService,
      private communitySvc: CommunityService,
      private toastr: ToastrService,
  ) {
    this.data = {
      title: 'Pasteleria Atesanal "Clark"',
      desc: '7.6 km',
      sharelike: true,
      image: 'https://mdbootstrap.com/img/new/standard/city/041.jpg'
    };

    this.data2 = Object.assign({}, this.data);
    this.data2.image = 'https://bootstrap-ecommerce.com/bootstrap-ecommerce-html/images/items/12.jpg';
    this.data2.desc = '$525 MXN';
  }

  ngOnInit(): void {
    this.loadCategories();
    this.loadBanner();
    this.loadBusiness();
    this.loadProducts();
    this.loadComunities();
    this.toastr.success('Post Publicado!');
  }

  loadCategories(){
    this.categorySvc.getCategories().subscribe((res: any) => {
          if (res && res.hasOwnProperty('count') && res.count > 0 ){
            this.categoriesList = res.results;
          }
        },
        error => {
          console.log(error);
        });
  }

  loadBusiness(){
    this.businessSvc.getAllBusines().subscribe((res: any) => {
          if (res && res.hasOwnProperty('count') && res.count > 0 ){
            this.businessList = res.results;
          }
        },
        error => {
          console.log(error);
        });
  }

  loadProducts(){
    this.productSvc.getAllProducts().subscribe((res: any) => {
          if (res && res.hasOwnProperty('count') && res.count > 0 ){
            this.productsList = res.results;
          }
        },
        error => {
          console.log(error);
        });
  }

  loadComunities(){
    this.communitySvc.getAllCommunities().subscribe((res: any) => {
          if (res && res.hasOwnProperty('count') && res.count > 0 ){
            this.communitiesList = res.results;
          }
        },
        error => {
          console.log(error);
        });
  }

  loadBanner(){
    this.categorySvc.getHomeBanner().subscribe((res: any) => {
          if (res && res.hasOwnProperty('count') && res.count > 0 ){
            this.banners = res.results;
          }
        },
        error => {
          console.log(error);
        });
  }

  ngAfterContentChecked() {
    const divsToHide: any = document.getElementsByClassName('owl-nav') as HTMLCollectionOf<HTMLElement>;
    for (let i = 0; i < divsToHide.length; i++){
      const node = divsToHide[i];
      node.style.visibility = 'hidden'; // or
      node.style.display = 'none';
    }
  }

}
