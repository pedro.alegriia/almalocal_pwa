import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-post-detail',
  templateUrl: './post-detail.component.html',
  styleUrls: ['./post-detail.component.sass']
})
export class PostDetailComponent implements OnInit {
  headerConf = {
    route: 'posts',
    title: 'Post'
  };

  sttylePost = { border: 'none' };

  constructor() { }

  ngOnInit(): void {
  }

}
