import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.sass']
})
export class SearchComponent implements OnInit {
  categoriesList:any;

  showModeResults: string;
  data: any;
  constructor() {
    this.showModeResults = 'map';
    this.categoriesList = ['Bienestar y salud','Restaurantes','Gratis','Ropay accesorios'];

    this.data = {
      title: 'Pasteleria Atesanal "Clark"',
      desc: '7.6 km',
      sharelike: true,
      image: 'https://mdbootstrap.com/img/new/standard/city/041.jpg'
    };
  }

  ngOnInit(): void {
  }

  changeModeShowResults(){
    if ( this.showModeResults == 'map'){
      this.showModeResults = 'cards';
    }else {
      this.showModeResults = 'map';
    }
  }

}
