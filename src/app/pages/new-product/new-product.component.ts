import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-new-product',
  templateUrl: './new-product.component.html',
  styleUrls: ['./new-product.component.sass']
})
export class NewProductComponent implements OnInit {
  headerConf = {
    route: 'profile',
    title: '+ Producto'
  };

  constructor() { }

  ngOnInit(): void {
  }

}
