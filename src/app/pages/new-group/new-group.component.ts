import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-new-group',
  templateUrl: './new-group.component.html',
  styleUrls: ['./new-group.component.sass']
})
export class NewGroupComponent implements OnInit {
  headerConf = {
    route: 'home',
    title: '+ Grupo'
  };

  constructor() { }

  ngOnInit(): void {
  }

}
