import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-notifications',
  templateUrl: './notifications.component.html',
  styleUrls: ['./notifications.component.sass']
})
export class NotificationsComponent implements OnInit {
  headerConf = {
    route: 'home',
    title: 'Mensajes'
  };

  constructor(private router: Router) { }

  ngOnInit(): void {
  }

  goToChat(idContact: number){
    this.router.navigate(['chat/'+ idContact]);
  }

}
