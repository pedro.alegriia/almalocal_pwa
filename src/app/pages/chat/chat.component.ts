import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.sass']
})
export class ChatComponent implements OnInit {
  headerConf = {
    route: 'notifications',
    title: 'Mermelada ca..'
  };

  constructor() { }

  ngOnInit(): void {
  }

}
