import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.sass']
})
export class ProfileComponent implements OnInit {
  headerConf = {
    route: 'home',
    title: 'Mi Perfil'
  };

  constructor(private router: Router) { }

  ngOnInit(): void {
  }

  logOut(): void{
    this.router.navigate(['/login']);
  }

  goTo(url: string): void{
    this.router.navigate(['/'+url]);
  }

}
