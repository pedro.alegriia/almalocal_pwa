import { Component, OnInit } from '@angular/core';
import { OwlOptions } from 'ngx-owl-carousel-o';
import {Router} from '@angular/router';

@Component({
  selector: 'app-groups',
  templateUrl: './groups.component.html',
  styleUrls: ['./groups.component.sass']
})
export class GroupsComponent implements OnInit {

  public customOptions: OwlOptions = {
    loop: false,
    mouseDrag: true,
    touchDrag: false,
    pullDrag: false,
    dots: false,
    navSpeed: 700,
    navText: ['', ''],
    responsive: {
      0: {
        items: 2
      },
      400: {
        items: 4
      },
      740: {
        items: 6
      },
      940: {
        items: 8
      }
    },
    nav: true
  };

  data: any = {};
  datatwo: any = {};
  constructor(private router: Router) {
    this.data = {
      title: 'Veganos en Guadalajara',
      sharelike: false,
      image: 'https://images.theconversation.com/files/179551/original/file-20170725-29149-e444p6.jpg?ixlib=rb-1.1.0&q=45&auto=format&w=926&fit=clip'
    };

    this.datatwo = Object.assign({},this.data);
    this.datatwo['image'] = 'https://www.unlockfood.ca/EatRightOntario/media/Website-images-resized/Tasty-meals-when-cooking-for-one-resized.jpg';
    this.datatwo['btn'] = true;
  }

  ngOnInit(): void {
  }

  showDetailGroup(id: number) {
    this.router.navigate(['detail-group/'+ id]);
  }

}
