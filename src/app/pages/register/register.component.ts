import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../../services/auth.service';
import { MustMatch } from '../../helpers/must-match.service';
import {Router} from '@angular/router';


@Component({
    selector: 'app-register',
    templateUrl: './register.component.html',
    styleUrls: ['./register.component.sass']
})
export class RegisterComponent implements OnInit {
    registerForm: FormGroup;

    constructor(
      public authService: AuthService,
      public formBuilder: FormBuilder,
      private router: Router
    ) {

        this.registerForm = this.formBuilder.group(
            {
                first_name: ["", [Validators.required]],
                last_name: ["", [Validators.required]],
                email: ["", [Validators.required]],
                password: ["", [Validators.required, Validators.minLength(6)]],
                confirmPassword: ["", Validators.required],
            },
            {
                validator: MustMatch("password", "confirmPassword"),
            }
        );

    }

    ngOnInit(): void {
    }

    register(){
        if (this.registerForm.invalid) {
            return;
        }

        const data = this.registerForm.value;
        this.authService.register(data).subscribe((res: any) => {
                console.log(res);
                if (res && res.hasOwnProperty('token')){
                    localStorage.setItem('token_alma',res['token']);
                    this.router.navigate(['/home']);
                }else {
                    alert(res['message']);
                }
            },
            error => {
                console.log(error)
          });
    }

}
