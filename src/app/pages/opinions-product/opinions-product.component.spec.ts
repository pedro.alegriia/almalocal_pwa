import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OpinionsProductComponent } from './opinions-product.component';

describe('OpinionsProductComponent', () => {
  let component: OpinionsProductComponent;
  let fixture: ComponentFixture<OpinionsProductComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OpinionsProductComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OpinionsProductComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
